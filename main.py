import DBus_helper

"""
Simple program to start every service I need for web development.
"""

dbusNginx = DBus_helper.DBusHelper("org.freedesktop.systemd1", "/org/freedesktop/systemd1/unit/nginx_2eservice")

dbusPhpFpm = DBus_helper.DBusHelper("org.freedesktop.systemd1", "/org/freedesktop/systemd1/unit/php_2dfpm_2eservice")

dbusSql = DBus_helper.DBusHelper("org.freedesktop.systemd1", "/org/freedesktop/systemd1/unit/mariadb_2eservice")


dbusNginx.start_service()

dbusPhpFpm.start_service()

dbusSql.start_service()
