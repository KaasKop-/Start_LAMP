import dbus


class DBusHelper:
    SysBus = dbus.SystemBus()
    bus_name = None
    bus_obj_path = None
    bus_obj = None

    def __init__(self, bus_name, bus_obj_path):
        self.bus_name = bus_name
        self.bus_obj_path = bus_obj_path
        try:
            self.bus_obj = self.SysBus.get_object(bus_name, bus_obj_path)
        except dbus.DBusException:
            print("It seems that the bus_name is incorrect or does not exist.")

    def get_state_of_service(self):
        prop_intf = dbus.Interface(self.bus_obj, "org.freedesktop.DBus.Properties")
        return prop_intf.Get("org.freedesktop.systemd1.Unit", "SubState")

    def start_service(self):
        manager = dbus.Interface(self.bus_obj, "org.freedesktop.systemd1.Unit")
        if self.get_state_of_service() == "dead":
            manager.Start("fail")
            return True
        return False

    def stop_service(self):
        manager = dbus.Interface(self.bus_obj, "org.freedesktop.systemd1.Unit")
        if self.get_state_of_service() == "running":
            manager.Stop("fail")
            return True
