import gi
from DBus_helper import DBusHelper

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class Gui(Gtk.Window):
    dbusNginx = DBusHelper("org.freedesktop.systemd1", "/org/freedesktop/systemd1/unit/nginx_2eservice")

    dbusPhpFpm = DBusHelper("org.freedesktop.systemd1", "/org/freedesktop/systemd1/unit/php_2dfpm_2eservice")

    dbusSql = DBusHelper("org.freedesktop.systemd1", "/org/freedesktop/systemd1/unit/mariadb_2eservice")

    def on_delete_window(self, *args):
        Gtk.main_quit(*args)

    def on_create_window(self, *args):
        pass

    def start_or_stop_nginx(self, *args):
        if args[0].get_active():
            self.dbusNginx.start_service()
        else:
            self.dbusNginx.stop_service()

    def start_or_stop_mariadb(self, *args):
        if args[0].get_active():
            self.dbusSql.start_service()
        else:
            self.dbusSql.stop_service()

    def start_or_stop_phpfpm(self, *args):
        if args[0].get_active():
            self.dbusPhpFpm.start_service()
        else:
            self.dbusPhpFpm.stop_service()


builder = Gtk.Builder()
builder.add_from_file("main_window.glade")

builder.connect_signals(Gui())

window = builder.get_object("MainWindow")
window.set_title("LAMP Starter")
window.set_resizable(False)
window.show_all()

Gtk.main()
